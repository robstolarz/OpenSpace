# Open Space #

**Join us on Discord!** [![Discord](https://discordapp.com/api/guilds/218889986047606785/widget.png)](https://discord.gg/Ys8U8Pt)]

An awesomely open space game.

## Building

Right now building is restricted to Linux. If you can manage to get it building on Windows, Awesome.

### Debian-based

Install the dependencies:

```
sudo apt-get install build-essential cmake libxrandr-dev libxinerama-dev libxcursor-dev xserver-xorg libglew-dev libopenvdb-dev libtbb-dev
```

Then building should be as simple as this:

```
cd build
cmake ..
make -j4 # replace this number with how many cpu cores you have
```
